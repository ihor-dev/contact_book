<?php

namespace App\Helpers;

use Illuminate\Support\Facades\Storage;

class ContactsBookHelper
{

    private static $filename;
    private static $perPage;

    public function __construct()
    {
        self::$filename = 'contacts.json';
        self::$perPage = 2;
    }

    public static function instance()
    {
        return new static();
    }

    /**
     * Get contacts by 
     *
     * @param [int] $page
     * @param [str] $sortBy
     * @param [bool] $desc
     * @return void
     */
    public static function getPaged($page, $sortBy, $desc)
    {
        return self::prepareContent($page, $sortBy, $desc, false);
    }

    /**
     * return whole sorted range from start
     *
     * @param [int] $page
     * @param [str] $sortBy
     * @param [bool] $desc
     * @return void
     */
    public static function getRange($page, $sortBy, $desc)
    {
        return self::prepareContent($page, $sortBy, $desc, true);
    }


    /**
     * return content
     *
     * @param [int] $page
     * @param [str] $sortBy
     * @param [bool] $desc
     * @param [bool] $range
     * @return void
     */
    private static function prepareContent($page, $sortBy, $desc, $range)
    {
        $data = self::readContacts();

        $filtered = $data->where('isActive', true);

        if($range)
        {
            $numRecords = $page * self::$perPage;
        
            $result = $filtered->slice(0,$numRecords);
        }
        else
        {
            $result = $filtered->forPage($page, self::$perPage);
        }
        
        $sorted = self::sort($sortBy, $desc, $result);

        $hasMore = self::hasMore($page);

        return [
            'dataset' => $sorted->values()->all(),
            'hasMore' => $hasMore,
        ];
    }

    /**
     * sort contact collection by given key
     *
     * @param [str] $sortBy
     * @param [bool] $desc
     * @param [str] $data
     * @return void
     */
    private static function sort($sortBy, $desc, $data)
    {
        $desc = ($desc === 'true' || $desc == true) ? true : false;

        $res = $data->sortBy($sortBy, SORT_REGULAR, $desc);

        return $res;
    }

    private static function readContacts()
    {
        $src = Storage::get( self::$filename );
        
        $srCollection = collect( json_decode($src) );
        
        $contacts = $srCollection->map(function($item, $key){
            return $item->item[0];
        });
        
        return $contacts;
    }

    private static function hasMore($currPage)
    {
        $data = self::readContacts();

        $totalPages = count($data) / self::$perPage;
        
        return $currPage < $totalPages;
    }
}