<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Helpers\ContactsBookHelper;

class ContactsController extends Controller
{
    public function getContacts(Request $r)
    {

        if($r->ajax())
        {

        
            if($r->filled('action') && $r->input('action') == 'sort')
            {
                $dataset = ContactsBookHelper::instance()->getRange($r->input('page'), $r->input('sortBy'), $r->input('sortDesc') == 'true');

                return response()->json($dataset);
            }

            if($r->filled('action') && $r->input('action') == 'loadMore')
            {
                $dataset = ContactsBookHelper::instance()->getPaged($r->input('page'), $r->input('sortBy'), $r->input('sortDesc') == 'true');

                return response()->json($dataset);
            }
        }

        $dataset = ContactsBookHelper::instance()->getPaged(0, 'name', false);
        
        return view('contacts', ['contacts' => $dataset]);
    }
}
