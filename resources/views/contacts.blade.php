@extends('layouts.main')

@section('content')

<div class="container-fluid mt-5" id="vue-container">
    <div class="row">
        <div class="col-sm-12">

            <contacts-component :contacts='@json($contacts)'></contacts-component>
        </div>
    </div>
</div>

@endsection