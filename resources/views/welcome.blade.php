@extends('layouts.main')

@section('content')

<div class="flex-center position-ref full-height">
    

    <div class="content">
        <div class="title m-b-md">
            Laravel Contact Book
        </div>

        <div class="links">
            <a href="{{ route('contacts') }}">Get Started For Free</a>
            
        </div>
    </div>
</div>

@endsection